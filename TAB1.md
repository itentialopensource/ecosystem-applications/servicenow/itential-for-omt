# Overview 

With Itential and ServiceNow, CSPs can not only accelerate their time to market but also introduce new and innovative services that stand out in a competitive market. Itential’s multi-domain orchestration platform enables CSPs to create unique services that can combine their network capabilities with third party services and capabilities, such as cloud, security, and interconnect. When combined with ServiceNow’s industry leading order management solution, CSPs can quickly define new services with the ability to scale order volumes and meet next-generation service complexity.


## Description

Our integrated approach to order fulfillment transforms traditional manual processes,  into a seamless, automated journey from initiation to activation. By leveraging the robust workflow and data management capabilities of ServiceNow’s OMT alongside Itential’s dynamic network integration and orchestration platform, we offer a tightly coupled and comprehensive solution that enables:

- End-to-end orchestration of network service lifecycle across multiple network technologies and vendors.
- The ability to dynamically publish any Itential workflow or service to the ServiceNow OMT Service Catalog.
- Tight integration with the TMF641 interface for service order fulfillment activities.
- ServiceNow OMT catalog owners to easily create new multi-domain products and services.
- Migrating current manual fulfillment steps to Itential automated workflows.
- Rapid onboarding of new vendors and emerging technologies to enable new services faster, with lower integration costs.

**This Ecosystem application is installed through the ServiceNow App Store. 
For details on installation and configuration, please visit the [ServiceNow Store Page](https://store.servicenow.com/sn_appstore_store.do#!/store/application/3918ffa54799c65068db98d4116d43ca).**

 
